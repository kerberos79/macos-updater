//
//  main.m
//  UCTalkUpdater
//
//  Created by keros on 26/10/2018.
//  Copyright © 2018 kerberos79. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
