//
//  WindowController.h
//  UCTalkUpdater
//
//  Created by keros on 30/10/2018.
//  Copyright © 2018 kerberos79. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface WindowController : NSWindowController

@end

NS_ASSUME_NONNULL_END
