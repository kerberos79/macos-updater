//
//  ViewController.h
//  UCTalkUpdater
//
//  Created by keros on 26/10/2018.
//  Copyright © 2018 kerberos79. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController
@property (weak) IBOutlet NSButton *launchButton;

- (IBAction)launchButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@end

