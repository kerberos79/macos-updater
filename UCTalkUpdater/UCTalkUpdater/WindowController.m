//
//  WindowController.m
//  UCTalkUpdater
//
//  Created by keros on 30/10/2018.
//  Copyright © 2018 kerberos79. All rights reserved.
//

#import "WindowController.h"

@interface WindowController ()

@end

@implementation WindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
