//
//  ViewController.m
//  UCTalkUpdater
//
//  Created by keros on 26/10/2018.
//  Copyright © 2018 kerberos79. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.launchButton setHighlighted:YES];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


- (IBAction)launchButtonTapped:(id)sender {
   [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/ucosx.app"];
    exit(0);
}

- (IBAction)cancelButtonTapped:(id)sender {
    exit(0);
}
@end
